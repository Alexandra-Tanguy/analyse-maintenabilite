# analyse maintenabilité



## Getting started

Exercice 1

1. Quelles sont les principales sources de complexité dans un système logiciel ?
La complexité dans un système logiciel peut arriver quand le code devient gros, il peut devenir difficile à suivre. Si chaque partie du code dépend des autres, cela peut rendre les modifications complexes, car un petit changement peut avoir des répercussions inattendues ailleurs. 
Quand le code est écrit avec beaucoup de conditions imbriquées ou de logique compliquée, cela peut être difficile à comprendre pour quelqu'un qui doit revenir dessus plus tard. 
Les problèmes de performance peuvent ajouter une autre couche de complexité, car il peut être difficile de savoir où se situent le problème et comment corriger sans casser d'autres parties du système.

2. Quels avantages y a-t-il à programmer vers une interface plutôt qu'une implémentation ?
Une interface apporte plusieurs avantages. Tout d'abord, elle rend le code plus flexible. Si besoin de changer l'implémentation d'une partie du système, c’est possible sans avoir à modifier tout le reste du code qui en dépend. 
L’interface rend le code plus réutilisable et plus facile à tester. Mais aussi plus maintenable, car il est plus facile de comprendre comment différentes parties du système interagissent quand elles communiquent à travers des interfaces.

3. Que signifie "D'abord, faites en sorte que ça fonctionne, ensuite assurez-vous que ce soit correct, et seulement après, préoccupez-vous de le rendre rapide." ?
Tout d'abord, l'objectif principal est de produire un logiciel qui fonctionne. 
Ensuite, une fois que le logiciel fonctionne, il est essentiel de corriger les bugs et de vérifier que le logiciel répond aux exigences et aux attentes des utilisateurs. 
Enfin, on peut se concentrer sur l'optimisation de sa performance. Cela peut impliquer l'identification des parties du code qui sont lentes 

4. Quelle est la meilleure méthode pour faire du refactoring ?

Avoir  des tests automatisés qui couvre le code existant garantit que les modifications apportées au code ne cassent pas les fonctionnalités existantes. 
Ensuite, on peut identifier les parties du code qui sont mal conçues, répétitives ou difficiles à comprendre, et les réorganiser pour les rendre plus claires et plus simples. Voir aussi si les principes de conception et les bonnes pratiques de programmation sont prise en compte  

Exercice 2

Le lien du repo git : https://gitlab.com/Alexandra-Tanguy/analyse-maintenabilite

Commit 1 : Ajout de la possibilité d'imprimer les factures au format JSON
Message : Dans ce premier commit, j'ai ajouté une option de format à la méthode invoice() qui permet de choisir entre le format texte et JSON. J'ai ensuite modifié la méthode pour construire la facture au format JSON si l'option est définie sur 'json'

Commit 2 : Permettre l'édition des types de voiture et de leurs règles de calcul de montant de location
Message : Dans ce commit, j'ai modifié la classe Car pour utiliser un dictionnaire price_rules pour stocker les règles de calcul du prix. Cela permet de modifier facilement les règles sans changer le code de la classe Car elle-même

Commit 3 : Calcul des points de fidélité en fonction du montant total de la location
Message : Dans ce dernier commit, j'ai ajouté le calcul des points de fidélité en fonction du montant total de la location, en utilisant la formule indiquée dans les spécifications du client. J'ai ensuite ajouté le nombre de points de fidélité à la facture, qu'elle soit au format texte ou JSON.
