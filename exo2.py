import json

class Car:
    def __init__(self, title, price_rules):
        self._title = title
        self._price_rules = price_rules

    def get_price_code(self):
        return self._price_rules['price_code']

    def get_title(self):
        return self._title

    def get_price_rules(self):
        return self._price_rules

    def set_price_rules(self, price_rules):
        self._price_rules = price_rules


class Rental:
    def __init__(self, car, days_rented):
        self._car = car
        self._days_rented = days_rented

    def get_days_rented(self):
        return self._days_rented

    def get_car(self):
        return self._car


class Customer:
    def __init__(self, name):
        self._name = name
        self._rentals = []

    def add_rental(self, rental):
        self._rentals.append(rental)

    def get_name(self):
        return self._name

    def _generate_text_invoice(self, result):
        text_result = f"Rental Record for {result['name']}\n"
        for rental in result["rentals"]:
            text_result += f"\t{rental['title']}\t{rental['amount']:.1f}\n"
        text_result += f"Amount owed is {result['total_amount']:.1f}\n"
        text_result += f"You earned {result['frequent_renter_points']} frequent renter points\n"
        text_result += f"You earned {result['loyalty_points']} loyalty points\n"
        return text_result

    def invoice(self, format='text'):
        total_amount = 0.0
        frequent_renter_points = 0
        result = {"name": self.get_name(), "rentals": []}

        for rental in self._rentals:
            this_amount = 0.0

            # Calcul des montants pour chaque type de voiture
            price_rules = rental.get_car().get_price_rules()
            if price_rules['type'] == 'regular':
                this_amount += price_rules['base_price'] + rental.get_days_rented() * price_rules['price_per_day']
                if rental.get_days_rented() > price_rules['threshold_days']:
                    this_amount -= (rental.get_days_rented() - price_rules['threshold_days']) * price_rules['discount_per_day']
            elif price_rules['type'] == 'new_model':
                this_amount += price_rules['base_price'] + rental.get_days_rented() * price_rules['price_per_day']
                if rental.get_days_rented() > price_rules['threshold_days']:
                    this_amount -= (rental.get_days_rented() - price_rules['threshold_days']) * price_rules['discount_per_day']

            # Calcul des points de fidélité
            frequent_renter_points += 1
            if price_rules['type'] == 'new_model' and rental.get_days_rented() > 1:
                frequent_renter_points += 1

            # Ajout des détails de location à la facture au format JSON
            result["rentals"].append({
                "title": rental.get_car().get_title(),
                "amount": round(this_amount / 100, 1)
            })

            total_amount += this_amount

        # Calcul du nombre de points de fidélité
        loyalty_points = int(total_amount * 0.1)

        # Ajout du montant total et du nombre de points de fidélité à la facture
        result["total_amount"] = round(total_amount / 100, 1)
        result["frequent_renter_points"] = frequent_renter_points
        result["loyalty_points"] = loyalty_points

        if format == 'json':
            return json.dumps(result, indent=4)
        else:
            # Format de facture textuel
            return self._generate_text_invoice(result)


# Exemples d'utilisation :
car1_rules = {'type': 'regular', 'base_price': 5000, 'price_per_day': 9500, 'threshold_days': 5, 'discount_per_day': 10000}
car2_rules = {'type': 'new_model', 'base_price': 9000, 'price_per_day': 15000, 'threshold_days': 3, 'discount_per_day': 10000}

car1 = Car("Car 1", car1_rules)
car2 = Car("Car 2", car2_rules)

rental1 = Rental(car1, 3)
rental2 = Rental(car2, 2)

customer = Customer("John Doe")
customer.add_rental(rental1)
customer.add_rental(rental2)

print(customer.invoice('json'))
print(customer.invoice('text'))
